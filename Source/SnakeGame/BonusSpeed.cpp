// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeed.h"
#include "SnakeBase.h"

ABonusSpeed::ABonusSpeed()
{
	PrimaryActorTick.bCanEverTick = true;
	//BnSpeed = false;
}

void ABonusSpeed::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABonusSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			//Snake->AddSnakeElement();
			Snake->MovementSpeed = Snake->MovementSpeed * 0.5f;
			Snake->SetActorTickInterval(Snake->MovementSpeed);

			// ������
			int RandomX = FMath::RandRange(-475, 485); // �������� X
			int RandomY = FMath::RandRange(-475, 485); // �������� Y
			this->SetActorLocation(FVector(RandomX, RandomY, -50)); // ���������� ���

			//Destroy(); // ����������� � ������, ����� ���� ���������� ����� ������.
		}
	}
}